  {
    "name": "Alambic Metrics",
    "version": "0.1",
    "children": [
      {
        "name": "Maximum depth of nesting",
        "mnemo": "NEST",
        "desc": [
	    "The maximum depth of nesting counts the highest number of imbricated code (including conditions and loops) in a function. ",
	    "Deeper nesting threatens understandability of code and induces more test cases to run the different branches. Practitioners usually consider that a function with three or more nested levels becomes significantly more difficult for the human mind to apprehend how it works."
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Number of files",
        "mnemo": "FILES",
        "desc": [
            "The number of code files (e.g. with a <code>.java</code> for the Java language) in the source code hierarchy. The SonarQube measure used for this purpose is <mark>files</mark>. This metric is often used as rule-of-thumb indicator for the size of software, and to make other measures relatives."
        ],
        "scale": [100,1000,10000,100000]
      },
      {
        "name": "Number of functions",
        "mnemo": "FUNCTIONS",
        "desc": [
            "The number of functions defined in the source files.",
            "The SonarQube measure used for this purpose is <mark>functions</mark>. This metric is often used as rule-of-thumb indicator for the size of software, and to make other measures relatives.",
	    "See also SonarQube's definitions for size metrics: <a href=\"http://docs.sonarqube.org/display/SONAR/Metric+definitions#Metricdefinitions-Size\">http://docs.sonarqube.org/display/SONAR/Metric+definitions#Metricdefinitions-Size</a>. More details on the metric: <a href=\"http://docs.sonarqube.org/display/SONAR/Metrics+-+Methods\">http://docs.sonarqube.org/display/SONAR/Metrics+-+Methods</a>."
        ],
        "scale": [100,1000,10000,100000]
      },
      {
        "name": "Number of classes",
        "mnemo": "CLASSES",
        "desc": [
            "The number of Java classes, including nested classes, interfaces, enums and annotations.",
            "The SonarQube measure used for this purpose is <mark>classes</mark>. This metric is often used as rule-of-thumb indicator for the size of software, and to make other measures relatives.",
	    "See also SonarQube's definitions for size metrics: <a href=\"http://docs.sonarqube.org/display/SONAR/Metric+definitions#Metricdefinitions-Size\">http://docs.sonarqube.org/display/SONAR/Metric+definitions#Metricdefinitions-Size</a>."
        ],
        "scale": [100,1000,10000,100000]
      },
      {
        "name": "Comment rate",
        "mnemo": "COMMENT_LINES_DENSITY",
        "desc": [
            "The ratio of source lines of code on the number of comment lines of code.",
            "Density of comment lines = Comment lines / (Lines of code + Comment lines) * 100. With such a formula, <var>50%</var> means that the number of lines of code equals the number of comment lines and <var>100%</var> means that the file only contains comment lines. The SonarQube measure used for this purpose is <mark>comment_lines_density</mark>. ",
	    "See also SonarQube's page on comment lines metrics: <a href=\"http://docs.sonarqube.org/display/SONAR/Metrics+-+Comment+lines\">http://docs.sonarqube.org/display/SONAR/Metrics+-+Comment+lines</a>."
        ],
        "scale": [10,15,20,30]
      },
      {
        "name": "Average number of attributes",
        "mnemo": "ATTRS",
        "desc": [
            "Average number of attributes defined in a class. This can be compared to the <a href=\"/documentation/metrics.html#TOPD\">total number of operands</a> (TOPD) considered at the class level, and is representative of the data complexity of the class (not including the complexity of methods)."
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Average number of methods",
        "mnemo": "METHS",
        "desc": [
            "Average number of methods defined in the class. This can be compared to the <a href=\"/documentation/metrics.html#TOPD\">total number of operators</a> (TOPT) considered at the class level."
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Public API",
        "mnemo": "PUBLIC_API",
        "desc": [
            "Number of public Classes + number of public Functions + number of public Properties. The SonarQube measure used for this purpose is <mark>public_api</mark>.",
	    "See also SonarQube's definition for size metrics: <a href=\"http://docs.sonarqube.org/display/SONAR/Metric+definitions#Metricdefinitions-Size\">http://docs.sonarqube.org/display/SONAR/Metric+definitions#Metricdefinitions-Size</a>. More details can be seen here: <a href=\"http://docs.sonarqube.org/display/SONAR/Metrics+-+Public+API\">http://docs.sonarqube.org/display/SONAR/Metrics+-+Public+API</a>"
        ],
        "scale": [48347,14018,6497.5,2420.75]
      },
      {
        "name": "Public documented API",
        "mnemo": "PUBLIC_API_DOC",
        "desc": [
            "The percentage of documented API accesses. Density of public documented API = (Public API - Public undocumented API) / Public API * 100. The SonarQube measure used for this purpose is <mark>public_documented_api_density</mark>.",
	    "See also SonarQube's definition for size metrics: <a href=\"http://docs.sonarqube.org/display/SONAR/Metric+definitions#Metricdefinitions-Size\">http://docs.sonarqube.org/display/SONAR/Metric+definitions#Metricdefinitions-Size</a>. "
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Average number of public attributes",
        "mnemo": "ATTRS_PUBLIC",
        "desc": [
            "Average number of attributes defined in classes with a <code>public</code> modifier.",
	    "Publicness of attributes is meaningful for reusability: if there are a lot of public attributes available, it may be more time-consuming to find the right one. Also, good practices for object-oriented programming recommend to use getters and setters in most cases."
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Average number of public methods",
        "mnemo": "METHS_PUBLIC",
        "desc": [
            "Average number of methods defined in classes with a <code>public</code> modifier.",
	    "Publicness of methods is meaningful for reusability: if there are a lot of public methods available, it may be more time-consuming to find the right one."
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Depth of Inheritance Tree",
        "mnemo": "MDIT",
        "desc": [
	    "The maximum depth of inheritance tree of a class within the inheritance hierarchy is defined as the maximum length from the considered class to the root of the class hierarchy tree and is measured by the number of ancestor classes. In cases involving multiple inheritance, the MDIT is the maximum length from the node to the root of the tree [<a href=\"/documentation/references.html#Chidamber1994\">Chidamber1994</a>].", 
	    "A deep inheritance tree makes the understanding of the object-oriented architecture difficult. Well structured OO systems have a forest of classes rather than one large inheritance lattice. The deeper the class is within the hierarchy, the greater the number of methods it is likely to inherit, making it more complex to predict its behavior and, therefore, more fault-prone [<a href=\"/documentation/references.html#Chidamber1994\">Chidamber1994</a>]. However, the deeper a particular tree is in a class, the greater potential reuse of inherited methods [<a href=\"/documentation/references.html#Chidamber1994\">Chidamber1994</a>].",
	    "See also SonarQube's page on the depth in tree: <a href=\"http://docs.sonarqube.org/display/SONAR/Metrics+-+Depth+in+Tree\">http://docs.sonarqube.org/display/SONAR/Metrics+-+Depth+in+Tree</a>"
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Source Lines Of Code",
        "mnemo": "NCLOC",
        "desc": [
            "Number of physical lines that contain at least one character which is neither a whitespace or a tabulation or part of a comment. This is mapped to SonarQube's <mark>ncloc</mark> metric.",
	    "See also SonarQube's definition for size metrics: <a href=\"http://docs.sonarqube.org/display/SONAR/Metric+definitions#Metricdefinitions-Size\">http://docs.sonarqube.org/display/SONAR/Metric+definitions#Metricdefinitions-Size</a>. More details can be seen here: <a href=\"http://docs.sonarqube.org/display/SONAR/Metrics+-+Lines+of+code\">http://docs.sonarqube.org/display/SONAR/Metrics+-+Lines+of+code</a>"
        ],
        "scale": [915828.0,198290.25,97961.5,38776.25]
      },
      {
        "name": "Cloning density",
        "mnemo": "DUPLICATED_LINES_DENSITY",
        "desc": [
            "The amount of duplicated lines in the code, divided by the number of lines. This is expressed as a percentage; The SonarQube metric used for this purpose is <mark>duplicated_lines_density</mark>.",
	    "See also SonarQube's definition for duplications: <a href=\"http://docs.sonarqube.org/display/SONAR/Metric+definitions#Metricdefinitions-Duplications\">http://docs.sonarqube.org/display/SONAR/Metric+definitions#Metricdefinitions-Duplications</a>. There is a longer description of the code cloning algorithm here: <a href=\"http://docs.sonarqube.org/display/SONAR/Duplications\">http://docs.sonarqube.org/display/SONAR/Duplications</a>."
        ],
        "scale": [50,40,30,10]
      },
      {
        "name": "Average Cyclomatic Complexity",
        "mnemo": "FUNCTION_COMPLEXITY",
        "desc": [
            "The average number of execution paths found in functions.",
	    "Basically, the counter is incremented every time the control flow of the function splits, with any function having at least a cyclomatic number of 1. The sum of cyclomatic numbers for all functions is then divided by the number of functions. In SonarQube the measure is <code>function_complexity</code>.",
	    "The cyclomatic number is a measure borrowed from graph theory and was introduced to software engineering by McCabe in [<a href=\"/documentation/references.html\">McCabe1976</a>]. It is defined as the number of linearly independent paths that comprise the program. To have good testability and maintainability, McCabe recommends that no program modules (or functions as for Java) should exceed a cyclomatic number of 10. It is primarily defined at the function level and is summed up for higher levels of artefacts.",
	    "See also Wikipedia's entry on cyclomatic complexity: <a href=\"http://en.wikipedia.org/wiki/Cyclomatic_complexity\">http://en.wikipedia.org/wiki/Cyclomatic_complexity</a>.",
	    "See also SonarQube's definition for code complexity: <a href=\"http://docs.sonarqube.org/display/SONAR/Metrics+-+Complexity\">http://docs.sonarqube.org/display/SONAR/Metrics+-+Complexity</a>. There is also a discussion about its meaning here: <a href=\"http://www.sonarqube.org/discussing-cyclomatic-complexity/\">http://www.sonarqube.org/discussing-cyclomatic-complexity/</a>",
	    "See also the Maisqual wiki for more details on complexity measures: <a href=\"http://maisqual.squoring.com/wiki/index.php/Category:Complexity_Metrics\">http://maisqual.squoring.com/wiki/index.php/Category:Complexity_Metrics</a>."
        ],
        "scale": [5.3,3.5,2.8,2.3]
      },
      {
        "name": "Number of non-conformities for analysability",
        "mnemo": "NCC_ANA",
        "desc": [
            "The total number of violations of rules that impact analysability in the source code.",
	    "See also the <a href=\"/documentation/rules.html\">page on practices</a> for a list of checked rules and their associated impact on quality characteristics."
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Number of non-conformities for changeability",
        "mnemo": "NCC_CHA",
        "desc": [
            "The total number of violations of rules that impact changeability in the source code.",
	    "See also the <a href=\"/documentation/rules.html\">page on practices</a> for a list of checked rules and their associated impact on quality characteristics."
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Number of non-conformities for reliability",
        "mnemo": "NCC_REL",
        "desc": [
            "The total number of violations of rules that impact reliability in the source code.",
	    "See also the <a href=\"/documentation/rules.html\">page on practices</a> for a list of checked rules and their associated impact on quality characteristics."
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Number of non-conformities for reusability",
        "mnemo": "NCC_REU",
        "desc": [
            "The total number of violations of rules that impact reusability in the source code.",
	    "See also the <a href=\"/documentation/rules.html\">page on practices</a> for a list of checked rules and their associated impact on quality characteristics."
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Average number of non-conformities for analysability",
        "mnemo": "NCC_ANA_IDX",
        "desc": [
            "The total number of violations of rules that impact analysability in the source code, divided by the number of thousands of lines of code.",
	    "See also the <a href=\"/documentation/rules.html\">page on practices</a> for a list of checked rules and their associated impact on quality characteristics."
        ],
        "scale": [1,0.5,0.3,0.1]
      },
      {
        "name": "Average number of non-conformities for changeability",
        "mnemo": "NCC_CHA_IDX",
        "desc": [
            "The total number of violations of rules that impact changeability in the source code, divided by the number of thousands of lines of code.",
	    "See also the <a href=\"/documentation/rules.html\">page on practices</a> for a list of checked rules and their associated impact on quality characteristics."
        ],
        "scale": [3,1,0.5,0.3]
      },
      {
        "name": "Average number of non-conformities for reliability",
        "mnemo": "NCC_REL_IDX",
        "desc": [
            "The total number of violations of rules that impact reliability in the source code, divided by the number of thousands of lines of code.",
	    "See also the <a href=\"/documentation/rules.html\">page on practices</a> for a list of checked rules and their associated impact on quality characteristics."
        ],
        "scale": [1,0.5,0.3,0.1]
      },
      {
        "name": "Average number of non-conformities for reusability",
        "mnemo": "NCC_REU_IDX",
        "desc": [
            "The total number of violations of rules that impact reusability in the source code, divided by the number of thousands of lines of code.",
	    "See also the <a href=\"/documentation/rules.html\">page on practices</a> for a list of checked rules and their associated impact on quality characteristics."
        ],
        "scale": [3,1,0.5,0.3]
      },
      {
        "name": "Number of violated rules for changeability",
        "mnemo": "RKO_CHA",
        "desc": [
            "The number of changeability rules that have at least one violation on the artefact."
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Number of rules for changeability",
        "mnemo": "RULES_CHA",
        "desc": [
            "The total number of rules checked for changeability."
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Adherence to changeability rules",
        "mnemo": "ROKR_CHA",
        "desc": [
            "Ratio of conform changeability practices on the number of checked changeability practices."
        ],
        "scale": [10,30,50,75]
      },
      {
        "name": "Number of violated rules for analysability",
        "mnemo": "RKO_ANA",
        "desc": [
            "The number of analysability rules that have at least one violation on the artefact."
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Number of rules for analysability",
        "mnemo": "RULES_ANA",
        "desc": [
            "The total number of rules checked for analysability."
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Adherence to analysability rules",
        "mnemo": "ROKR_ANA",
        "desc": [
            "Ratio of conform analysability practices on the number of checked analysability practices."
        ],
        "scale": [10,30,50,75]
      },
      {
        "name": "Number of violated rules for reliability",
        "mnemo": "RKO_REL",
        "desc": [
            "The number of reliability rules that have at least one violation on the artefact."
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Number of rules for reliability",
        "mnemo": "RULES_REL",
        "desc": [
            "The total number of rules checked for reliability."
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Adherence to reliability rules",
        "mnemo": "ROKR_REL",
        "desc": [
            "Ratio of conform reliability practices on the number of checked reliability practices."
        ],
        "scale": [10,30,50,75]
      },
      {
        "name": "Number of violated rules for reusability",
        "mnemo": "RKO_REU",
        "desc": [
            "The number of reusability rules that have at least one violation on the artefact."
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Number of rules for reusability",
        "mnemo": "RULES_REU",
        "desc": [
            "The total number of rules checked for reusability."
        ],
        "scale": [1,2,3,4]
      },
      {
        "name": "Adherence to reusability rules",
        "mnemo": "ROKR_REU",
        "desc": [
            "Ratio of conform reusability practices on the number of checked reusability practices."
        ],
        "scale": [10,30,50,75]
      },
      {
        "name": "Percentage of lines of code covered by tests",
        "mnemo": "LINE_COVERAGE",
        "desc": [
            "The percentage of source lines of code that is exercised by the tests. The SonarQube measure used for this purpose is <mark>line_coverage</mark>."
        ],
        "scale": [0,20,48.89,87.70]
      },
      {
        "name": "Percentage of branches covered by tests",
        "mnemo": "BRANCH_COVERAGE",
        "desc": [
            "The percentage of branches that is exercised by the tests. The SonarQube measure used for this purpose is <mark>branch_coverage</mark>.",
            "On a given line of code, Line coverage simply answers the following question: Has this line of code been executed during the execution of the unit tests?"
        ],
        "scale": [0,15,36.5,73.299999999999997]
      },
      {
        "name": "Number of tests relative to the code size",
        "mnemo": "TST_VOL_IDX",
        "desc": [
            "The total number of test cases for the product, divided by the number of thousands of SLOC.",
            "Metric is computed from SonarQube <mark>tests</mark> and <mark>ncloc</mark> metrics."
        ],
        "scale": [2,5,7,10]
      },
      {
        "name": "Test success density",
        "mnemo": "TEST_SUCCESS_DENSITY",
        "desc": [
            "The percentage of failed tests during the last execution of the test plan. The SonarQube measure used for this purpose is <mark>test_success_density</mark>.",
            "Computation is as follows: Test success density = (Unit tests - (Unit test errors + Unit test failures)) / Unit tests * 100, where Unit test errors is the number of unit tests that have failed, Unit test failures is the number of unit tests that have failed with an unexpected exception."
        ],
        "scale": [50,65,80,95]
      }
    ]
  }
