---
output: html_document
---

```{r init, echo=FALSE, message=FALSE}
library(dygraphs)
library(xts)

#project.id <- 'tools.cdt'
file.evol = paste("", project.id, "_its_evol.csv", sep="")
evol <- read.csv(file=file.evol, header=T)
evol[is.na(evol)] <- 0
evol <- evol[cumsum(abs(evol$changed + evol$changers + evol$closed 
                        + evol$closers + evol$opened + evol$openers 
                        + evol$trackers)) != 0,]

evol.xts <- xts(evol, order.by = as.POSIXct(evol$unixtime, origin="1970-01-01"))
p <-dygraph(evol.xts[,c('changers', 'openers', 'closers')],
        main = paste('Changers, Openers, and Closers for ', project.id, sep=''),
        width = 800, height = 250 ) %>% 
      dyRangeSelector()
p
```

