---
output: html_document
---
<div class="row"><div class="col-md-12">
```{r init, echo=FALSE, fig.height=3, message=FALSE}
library(plotly)
library(reshape2)

#project.id <- 'tools.pdt'
file.evol = paste("", project.id, "_scm_evol.csv", sep="")
evol <- read.csv(file=file.evol, header=T)
evol[is.na(evol)] <- 0
evol <- evol[cumsum(abs(evol$authors + evol$added_lines + evol$removed_lines 
                        + evol$commits + evol$committers + evol$repositories)) != 0,]

my.data.long <- melt(evol[,c(1,3,6:8)], id.vars="date", value.name="value", variable.name="metric")
p <-plot_ly(my.data.long, x = date, y = metric, size = value, mode = "markers", width=800, height=350) %>%
      layout(autosize = F, width=800, height=350)
p
```
</div></div>
