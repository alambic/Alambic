---
output: html_document
---

```{r init, echo=FALSE, message=FALSE}
library(dygraphs)
library(xts)

#project.id <- 'modeling.sirius'
file.evol = paste("", project.id, "_scm_evol.csv", sep="")
evol <- read.csv(file=file.evol, header=T)
evol[is.na(evol)] <- 0
evol <- evol[cumsum(abs(evol$authors + evol$added_lines + evol$removed_lines 
                        + evol$commits + evol$committers + evol$repositories)) != 0,]

evol.xts <- xts(evol, order.by = as.POSIXct(evol$unixtime, origin="1970-01-01"))
p <-dygraph(evol.xts[,c('commits')],
        main = paste('Commits for ', project.id, sep=''),
        width = 800, height = 250 ) %>% 
      dyRangeSelector()
p
```

